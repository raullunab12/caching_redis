# encoding: UTF-8
require 'redis'
require './movie'
require 'json'
#
class MovieCache
  MOVIES_KEY = "movies-#{Time.now.strftime("%d/%m/%Y")}"
  def self.all
    redis = Redis.new
    return JSON.parse(redis.get(MOVIES_KEY)) if redis.exists MOVIES_KEY
    movies = Movie.all.to_json
    redis.set(MOVIES_KEY, movies)
    JSON.parse(movies)
  end
end
