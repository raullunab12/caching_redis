# encoding: UTF-8
require 'data_mapper'
require 'dm-migrations'
require './movie'
require './movie_cache'

DataMapper::Logger.new($stdout, :debug)
DataMapper.setup(
  :default,
  'postgres://postgres:@localhost/caching_redis'
)

DataMapper.finalize

puts '****************************'
puts MovieCache.all
